#   proketSteward

## 介绍
(○｀（●●）´○)ノi

小猪管家，是一个监控程序，含有实时监测进程、文件字符串过滤、告警等功能；
纯python开发，基于python3。

1、实时监控日志文件，过滤定义的关键字，并及时告警；

2、监控系统cpu、内存、磁盘的使用情况，达到配置的阈值触发告警；

3、日志切割。监控指定文件，达到文件阈值大小，进行备份、新建，防止单个大文件出现；

4、监控指定进程的进程数量，低于规定值时发送告警信息。

## 功能说明
1. 监控日志中的字段

监控日志中的关键字，如果日志中出现了预定的关键字，则会将关键字所在行和上下行一同发送告警或记录进日志；

比如：监控nginx的access.log日志，定义出现 error 字段则发送告警，当后续更新的日志某行中包含了 error 字段，则触发告警；

2. 监控系统参数

监控系统的cpu、内存、磁盘占用，达到配置文件中配置的阈值后进行告警；

按照配置文件配置想要监控的项，监控cpu占用还是内存使用，或者是磁盘占用；当达到配置文件中预定的阈值时发送告警信息；

3. 日志切割

监控日志文件的大小，达到配置的大小后进行备份、新建，实现日志切割的功能；

按照配置文件中定义的目标文件、目标文件的文件大小上限，对目标文件进行循环监控，文件的大小达到预定上限时，进行备份、切割，防止单个文件过大；

4. 监控进程

监控指定的进程，如果目标进程的进程数低于阈值，则发送告警，达到进程宕掉后可以及时通知的效果；

示例：配置了java进程数不得低于1个，程序会通过 ps -ef 命令循环查看java进程是否存在，如果数量低于1，则会触发告警，从而发送告警信息；

## 软件架构

```
[root@xpt ~]# tree proketSteward/
proketSteward/
├── bin								#实际执行的代码存放目录
│   ├── basics_class.py					#基础类,定义了一些常用的方法,其他代码需要时候即可引入、调用
│   ├── filtration_log_old.py			#旧版的监控日志字段的文件,没有任何作用,用于保留
│   ├── filtration_log.py				#新版的监控日志字段的代码,实际执行的代码
│   ├── init_action.sh					#入口脚本,用于整个服务启动、关闭、状态查看
│   ├── logFile_size.py					#监控文件大小,对文件进行日志切割的代码文件
│   ├── process_monitoring.py			#监控进程的代码文件
│   ├── send_info.py					#发送告警消息的代码文件,定义了常用的告警类型:邮件、企业微信等
│   ├── system_parameter_action.py		#监控系统参数的实际执行文件
│   ├── system_parameter.py				#定义了监控系统参数的常用方法,如:cpu、内存、磁盘等,在实际执行py文件中被调用
│   └── test.py							#编辑代码时的测试文件,你不要管它就行
├── conf							#配置文件目录
│   ├── global.conf						#全局配置文件
│   └── send_info.conf					#发送告警消息的配置文件
├── LICENSE							#开源许可协议
├── logs							#日志存放目录
│   ├── filtration_log.log
│   └── messages.log
├── pids							#服务启动后进程的pid文件存放目录
│   └── README.txt
├── README.md						#自述文件
└── requirements.txt				#python依赖包
```


## 安装教程

1.  编译安装python3 (python3.6 ~ 3.9 都行)

    官方下载地址：`https://www.python.org/downloads/`

    安装教程,参考链接：
    
    http://note.youdao.com/noteshare?id=3ce3e3ca9fa3b04b8715c6516f850006&sub=7D093F13E9164C9A8F7E21115B8B7CC2

2.  安装 requirements.txt 中的ptyhon库

    `pip3 install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/`

    务必要使用第一步中编译好的python中的pip3。因为系统可能默认自带了pip，所以导致pip安装的包并不是运行python3时所使用的包;

3.  编辑 conf/ 中的配置文件,开启想要运行的功能并配置对应参数

4.  启动脚本

```
    sh bin/init_action.sh start
```


## 使用说明
1. 配置 conf/global.conf 内的 python_path 参数,编辑为实际的python3绝对路径,启动脚本中会用到,否则无法启动服务。

2. 配置 conf/global.conf 中需要启动的模块的功能

3. 启动、关闭、查看状态
```
[root@skip proketSteward]# sh bin/init_action.sh --help
Please execute correctly:
   |
   |___sh bin/init_action.sh start
   |   |___启动服务
   |
   |___sh bin/init_action.sh stop
   |   |___关闭服务
   |
   |___sh bin/init_action.sh status
       |___查看服务状态
```
## cong/send_info.conf 配置文件

> 以为监控以告警为主，需要配置告警媒介，使用微信告警，还是邮件告警，或者其他告警方式

##### 企业微信和邮件告警配置

一、配置 [weChat] 模块

1、登录企业微信
![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084108_f846bb31_2252737.png "屏幕截图.png")

2、企业 corpid

点击 我的企业：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084206_d2c5d01c_2252737.png "屏幕截图.png")

3、企业 corpsecret 和 agentid

点击 应用管理：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084243_035f37e8_2252737.png "屏幕截图.png")

查看参数：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084308_55115576_2252737.png "屏幕截图.png")

二、配置 [mail] 模块

以163为例

![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084528_9bf1a2e9_2252737.png "屏幕截图.png")

开启 SMTP

![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084651_daae0394_2252737.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0601/084751_4675bf51_2252737.png "屏幕截图.png")

## cong/global.conf 配置文件

该配置文件分为几个模块，分别用 [] 代表；

[global] 全局模块，配置使用的python的环境变量

[filtration_log] 日志文件监控模块

[cpu_info]、[mem_info]、[disk_info] 都属于系统性能监控模块

[log_file_cutting] 日志切割模块

[process_monitoring] 进程监控模块

模块中的参数主要分为：

```
switch --- 开关，用于配置该功能是否开启；
interval --- 检测时间间隔，每隔几秒循环一次，可以针对不同模块进行配置；比如磁盘，可以适当加大循环的时间间隔，因为磁盘增长一般不大，没必要频繁的检测；
send_info_mode --- 告警媒介，使用微信还是邮件告警？
sleep_time --- 告警后的休眠时间，告警后会进入休眠，防止频繁发送告警；
critical_value --- 阈值，低于此参数值会触发告警；
```

以上参数基本为共有的参数，其他一些参数在各自的模块中都会有注释说明；


## 作者
联系QQ: 992824390 （星火燎愿）