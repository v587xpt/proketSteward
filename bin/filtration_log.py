import time,subprocess,datetime,os
import sys
from basics_class import Basics_def
from send_info import Send_info
# -*- coding: utf-8 -*-

"""
功能:监控日志中自定义的关键字,当检测到关键字时发送告警.
执行日志写入了 logs/filtration_log.log 中;
开启的告警记录日志写入了 logs/filtration_log_Send_info.log 中;
"""

class Filtration_def():
    def __init__(self):
        self.basedir = os.path.abspath(os.path.dirname(__file__))
        self.basedir_end = self.basedir[0:-3]   #项目路径

        #解析配置文件,获取参数值
        class_Basics_def = Basics_def()
        self.result_aaaaa = class_Basics_def.analysis_parameter(self.basedir_end+"conf/global.conf","filtration_log") #解析哪个文件
        self.result_send_info = class_Basics_def.analysis_parameter(self.basedir_end+"conf/send_info.conf","global") #解析哪个文件
        #初始化,获取配置文件参数
        self.switch = self.result_aaaaa['switch']       #开关
        self.file_path = self.result_aaaaa['log_path']  #要检测的日志文件的绝对路径
        self.interval_time = self.result_aaaaa['interval_time']     #检测的频率(时间间隔)
        self.write_log_switch = self.result_aaaaa['write_log_switch'] #是否记录告警日志
        self.send_info_switch = self.result_aaaaa['send_info_switch']    #是否启用告警
        self.send_info_mode = self.result_aaaaa['send_info_mode']   #告警的方式
        self.only_name = self.result_send_info['only_name']     #读取发送告警消息的唯一标识


    # 执行shell命令的方法，调用Exec即可;
    def runWithOutLog(self,cmd):
        p = subprocess.Popen(cmd, shell=True, close_fds=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        return (p.returncode, stdout, stderr)
    def run(self,cmd):
        returncode, stdout, stderr = Filtration_def.runWithOutLog(self,cmd)
        return (returncode, stdout.strip(), stderr.strip())
    def Exec(self,cmd):
        ISOTIMEFORMAT = '%Y-%m-%d %X'
        reportTime = time.strftime(ISOTIMEFORMAT, time.localtime())
        retcode, stdout, stderr = Filtration_def.run(self,cmd)
        print('[%s] exec cmd[%s] stdout[%s] stderr[%s]' % (reportTime, cmd, str(stdout), str(stderr)))
        return stdout, stderr
    def Exectt(self,cmd):
        retcode, stdout, stderr = Filtration_def.run(self,cmd)
        return stdout.decode('utf-8'), stderr

    # 获取指定文件的最后一行的行号
    def get_lines_new(self,file_path):
        stdout,stderr = Filtration_def.Exectt(self,"cat %s | wc -l" % (file_path))
        return stdout

    #主程序:循环检测
    def main_test(self):

        interval_time = int(self.interval_time)
        # print(interval_time)

        # 先进行一次行号检测(前提是文件必须存在,进入循环后被检测文件可以不存在)
        start_numbers = Filtration_def.get_lines_new(self,self.file_path)  # 初次检测，获取行号；
        # print(start_numbers)
        time.sleep(interval_time)
        stop_numbers = Filtration_def.get_lines_new(self,self.file_path)  # 休眠后再次检测，获取行号；
        # print(stop_numbers)

        #执行前判断开关是否协调,防止开启进程后没有日志记录也没有告警,没有意义
        if self.write_log_switch != "on" and self.send_info_switch != "on":
            print("write_log_switch 和 send_info_switch 至少有一个要打开...exit()")
            exit()

        # 进入循环
        while True:
            # 只有当 start_numbers<stop_numbers 时才进行新增内容的获取判断
            if int(start_numbers) < int(stop_numbers):
                time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
                print(time_stamp + " " + "有新增内容,开始检测关键字...")

                dic_new_info = {}  # 定义字典，存储文件变动的内容，行号是键，内容是值；
                # 获取变动的内容
                out_yes, out_no = Filtration_def.Exectt(self,"sed -n '%s,%sp' %s" % (int(start_numbers) + 1, int(stop_numbers), self.file_path))
                # 将变动的 内容和行号 存入字典，行号是键，内容是值；
                hangHao = int(start_numbers) + 1
                for i in out_yes.split('\n'):
                    dic_new_info[hangHao] = i
                    hangHao += 1
                # print(dic_new_info)     #输出编排后的字典数据

                list_new_info = []  # 定义数组，存储含有关键字的行，以及其上下行
                for iu in dic_new_info:  # 遍历字典
                    # 判断关键字
                    if "Exception" in dic_new_info[iu] and "调用" not in dic_new_info[iu] and "customException" not in dic_new_info[iu] and "IllegalArgumentException" not in dic_new_info[iu] and "SocketTimeoutException" not in dic_new_info[iu] and "java.io.IOException: Broken pipe" not in dic_new_info[iu] and "Connection reset by peer" not in dic_new_info[iu] and "高峰期" not in dic_new_info[iu]:
                        """一旦过滤到匹配行，就将该行连同上下行一起存储到数组中"""
                        if iu - 1 not in list_new_info:
                            list_new_info.append(iu - 1)
                        if iu not in list_new_info:
                            list_new_info.append(iu)
                        if iu + 1 not in list_new_info:
                            list_new_info.append(iu + 1)
                    else:  # 过滤不到定义的字符串，则pass通过
                        pass
                # print(list_new_info)

                if len(list_new_info) != 0:
                    print("发现关键字,组装 error_info 消息...")
                    # 读取这些行，并组成段落
                    error_info = ""
                    for inum in list_new_info:
                        m_one_yes, m_one_no = Filtration_def.Exectt(self,"sed -n '%s,%sp' %s" % (inum, inum, self.file_path))
                        error_info = error_info + "[" + str(inum) + "行] " + m_one_yes + "\n"

                    time_stamp = datetime.datetime.now()
                    last_info = "主机：" + self.only_name + "\n" + \
                                "时间：" + time_stamp.strftime('%Y.%m.%d-%H:%M:%S') + "\n" + \
                                "告警消息：" + "\n" + error_info

                    # print(last_info)
                    #判断是否启用了日志记录,并输出到记录日志中
                    if str(self.write_log_switch) == "on":
                        print("告警消息,写入日志...")
                        f_log = open(self.basedir_end + "logs/filtration_log_Send_info.log", "a")
                        f_log.write(last_info)
                        f_log.close()
                    #判断是否启用了告警,并发送告警
                    if str(self.send_info_switch) == "on":
                        class_Send_info = Send_info()   #实例化
                        if str(self.send_info_mode) == "weChat":     #配置企业微信发送
                            class_Send_info.send_weChat(last_info)
                        if str(self.send_info_mode) == "mail":       #配置邮件发送
                            class_Send_info.send_mail(last_info)



                # 更新行号
                try:
                    start_numbers = int(stop_numbers)  # 更新初始行号
                    time.sleep(interval_time)
                    stop_numbers = Filtration_def.get_lines_new(self,self.file_path)  # 休眠后重新获取新的行号
                except:
                    # 当检测中出现报错时,通过定义错误的值,引导程序进入else中
                    time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
                    print(time_stamp + " " + "行号获取时出现错误")
                    start_numbers = 0
                    stop_numbers = 0

            else:
                time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
                # print(time_stamp + " " + "不满足判断条件,重新检测...")
                print(time_stamp + " " + "没有新增内容,跳过...")
                try:
                    start_numbers = Filtration_def.get_lines_new(self,self.file_path)  # 更新初始行号
                    time.sleep(interval_time)
                    stop_numbers = Filtration_def.get_lines_new(self,self.file_path)  # 休眠后重新获取新的行号
                except:
                    # 当检测中出现报错时,通过定义错误的值,引导程序进入else中
                    start_numbers = 0
                    stop_numbers = 0

            # 开始日志收集：所有输出写入log文件
            file_log = open(self.basedir_end + "logs/filtration_log.log", "a")
            sys.stdout = file_log
            sys.stderr = file_log


if __name__ == "__main__":
    admin = Filtration_def()

    if admin.switch == "on":
        # 获取自身的进程号,用于关闭时使用
        pid = os.getpid()
        # print('进程id : %d' % pid)
        f = open(admin.basedir_end + "pids/filtration_log.pid", "w")
        f.write(str(pid))
        f.close()

        admin.main_test()
