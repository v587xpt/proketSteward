from system_parameter import System_item
from basics_class import Basics_def
import datetime,time,os
from send_info import Send_info
import threading
import numpy as np

"""实际的检测动作,检测系统cpu、内存、磁盘的使用情况,当超过阈值时发送告警"""

class System_parameter_action():
    def __init__(self):
        self.basedir = os.path.abspath(os.path.dirname(__file__))
        self.basedir_end = self.basedir[0:-3]  # 项目路径

        self.system_item = System_item()  # 实例化获取系统参数的类
        self.class_Basics_def = Basics_def()    #实例化基础方法的类

        self.class_Send_info = Send_info()  #实例化发送消息的类

        # 获取发送消息的唯一主机标识
        self.result_send_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/send_info.conf","global")  # 解析哪个文件
        self.only_name = self.result_send_info['only_name']

        #获取配置文件内所有参数的值
        # ---1.获取cpu的配置参数
        try:
            self.result_cpu_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/global.conf", "cpu_info")
            self.cpu_info_give_an_alarm = self.result_cpu_info["switch"]
            self.cpu_info_sendInfo_mode = self.result_cpu_info["send_info_mode"]
            self.cpu_info_interval = self.result_cpu_info['interval']
            self.cpu_info_critical_value = self.result_cpu_info['critical_value']
            self.cpu_info_continuous_number = self.result_cpu_info['continuous_number']
            self.cpu_info_sleep_time = self.result_cpu_info['sleep_time']
        except Exception as e:
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            print(time_stamp + " " + "cpu_info 解析失败,请检查配置文件参数.")
            exit()

        #---2.获取内存的配置参数
        try:
            self.result_mem_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/global.conf", "mem_info")
            self.mem_info_give_an_alarm = self.result_mem_info["switch"]
            self.mem_info_sendInfo_mode = self.result_mem_info["send_info_mode"]
            self.mem_info_interval = self.result_mem_info['interval']
            self.mem_info_critical_value = self.result_mem_info['critical_value']
            self.mem_info_continuous_number = self.result_mem_info['continuous_number']
            self.mem_info_sleep_time = self.result_mem_info['sleep_time']
        except:
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            print(time_stamp + " " + "mem_info 解析失败,请检查配置文件参数.")
            exit()
        #---3.获取磁盘的配置参数
        try:
            self.result_disk_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/global.conf", "disk_info")
            self.disk_info_switch = self.result_disk_info['switch']
            self.disk_info_disk_zone = self.result_disk_info['disk_zone']   #要监控的目录
            self.disk_info_interval = self.result_disk_info['interval']     #监测时间间隔
            self.disk_info_critical_value = self.result_disk_info['critical_value']     #阈值
            self.disk_info_continuous_number = self.result_disk_info['continuous_number']   #连续达到阈值几次才告警
            self.disk_info_send_info_mode = self.result_disk_info['send_info_mode'] #告警媒介
            self.disk_info_sleep_time = self.result_disk_info['sleep_time']
        except:
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            print(time_stamp + " " + "disk_info 解析失败,请检查配置文件参数.")
            exit()


    def get_cpu_result(self):
        """检测cpu使用"""
        interval = self.cpu_info_interval       #检测时间间隔
        critical_value = self.cpu_info_critical_value   #阈值
        continuous_number = self.cpu_info_continuous_number #告警阈值(连续几次)

        while True:
            cpu_list = []
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            for i in range(0, int(continuous_number)):  # 连续几次的检测
                get_cpu_result = self.system_item.get_cpu(int(interval))  # 获取cpu占用百分比
                # print(get_cpu_result)       #后期添加配置/方法,可以将数据存储到数据库中;
                if get_cpu_result >= int(critical_value):
                    cpu_list.append(get_cpu_result)
                else:
                    pass

            if len(cpu_list) == int(continuous_number):
                #触发告警,组装消息,判断是企业微信还是邮件后发送
                last_info = "主机：" + self.only_name + "\n" + \
                            "时间：" + time_stamp + "\n" + \
                            "告警消息：" + "cpu使用率连续 " + str(self.cpu_info_continuous_number) +" 次达到阈值" + str(self.cpu_info_critical_value)
                if self.cpu_info_sendInfo_mode == "weChat":     #发送企业微信告警
                    self.class_Send_info.send_weChat(last_info)
                if self.cpu_info_sendInfo_mode == "mail":       #发送邮件告警
                    self.class_Send_info.send_mail(last_info)
                #发送告警后休眠(防止太吵)
                time.sleep(int(self.cpu_info_sleep_time))
            else:
                pass    #未达到阈值,进行下一轮的监测

    def get_mem_result(self):
        # 解析配置文件
        interval = self.mem_info_interval       #监测时间间隔
        critical_value = self.mem_info_critical_value       #阈值
        continuous_number = self.mem_info_continuous_number #告警阈值(连续几次)

        while True:
            mem_list = []
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            for i in range(0, int(continuous_number)):  # 连续几次的检测
                mem_total,mem_used,mem_free,usage_rate = self.system_item.get_mem()  # 获取mem的使用情况
                # print(str(mem_total)+" "+str(mem_used)+" "+str(mem_free)+" "+str(usage_rate))
                if usage_rate >= int(critical_value):
                    mem_list.append(usage_rate)
                else:
                    pass
                time.sleep(int(interval))   #利用休眠实现监测的时间间隔

            if len(mem_list) == int(continuous_number):
                last_info = "主机：" + self.only_name + "\n" + \
                            "时间：" + time_stamp + "\n" + \
                            "告警消息：" + "mem占用连续 " + str(self.mem_info_continuous_number) + " 次达到阈值" + str(self.mem_info_critical_value)
                if self.mem_info_sendInfo_mode == "weChat":  # 发送企业微信告警
                    self.class_Send_info.send_weChat(last_info)
                if self.mem_info_sendInfo_mode == "mail":  # 发送邮件告警
                    self.class_Send_info.send_mail(last_info)
                # 发送告警后休眠(防止太吵)
                time.sleep(int(self.mem_info_sleep_time))
            else:
                pass

    def get_disk_result(self):
        # 解析配置文件
        disk_zone = self.disk_info_disk_zone    #要监控的目录
        # print(disk_zone)
        disk_zone_list = disk_zone.split(',')   #字符串分割

        interval = self.disk_info_interval  #间隔时间间隔
        critical_value = self.disk_info_critical_value  #阈值
        continuous_number = self.disk_info_continuous_number    #连续几次才告警
        while True:
            disk_list = []

            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            for i in range(0, int(continuous_number)):  # 连续几次的检测
                for lili in disk_zone_list:
                    disk_total, disk_used, disk_free, disk_percent = self.system_item.get_disk(lili)
                    print(str(disk_total) + " " + str(disk_used) + " " + str(disk_free) + " " + str(disk_percent) + " " + str(lili))

                    disk_list.append(disk_percent)
                time.sleep(int(interval))  # 间隔时间(休眠)

            #一轮的检测完毕,将数组转换为矩阵
            disk_list_numpy = np.array(disk_list)
            # 矩阵转换为行和列显示
            numpy_np = disk_list_numpy.reshape([int(continuous_number), len(disk_zone_list)])
            print(numpy_np)     #输出行和列显示的矩阵
            for num in range(len(disk_zone_list)):
                arr_list = numpy_np[0:int(continuous_number), num].tolist()      #截取矩阵中的列,并转换为数组
                #数组排序,然后对比第一个数值的大小即可判断整个数组的大小
                arr_list.sort()
                if arr_list[0] >= int(critical_value):
                    #整个数组的值都大于阈值,发送告警
                    last_info = "主机：" + self.only_name + "\n" + \
                                "时间：" + time_stamp + "\n" + \
                                "告警消息：" + "disk磁盘占用连续 " + str(self.disk_info_continuous_number) + " 次达到阈值" + str(self.disk_info_critical_value)
                    if self.disk_info_send_info_mode == "weChat":  # 发送企业微信告警
                        self.class_Send_info.send_weChat(last_info)
                    if self.disk_info_send_info_mode == "mail":  # 发送邮件告警
                        self.class_Send_info.send_mail(last_info)
                    # 发送告警后休眠(防止太吵)
                    time.sleep(int(self.disk_info_sleep_time))

                    break       #有任何一个达到告警阈值,则跳出这个循环



if __name__ == "__main__":
    shilihua = System_parameter_action()        #实例化

    #---判断开关,是否开启对应的线程---#
    if shilihua.cpu_info_give_an_alarm == "on":      #开启了cpu监控的告警
        #---开启线程运行cpu的监控
        th_get_cpu_result = threading.Thread(target=shilihua.get_cpu_result).start()

    if shilihua.mem_info_give_an_alarm == "on":  # 开启了cpu监控的告警
        # ---开启线程运行mem的监控
        th_get_mem_result = threading.Thread(target=shilihua.get_mem_result).start()

    if shilihua.disk_info_switch == "on":  # 开启了cpu监控的告警
        # ---开启线程运行disk的监控
        th_get_disk_result = threading.Thread(target=shilihua.get_disk_result).start()

    #---判断是否生成pid文件
    if shilihua.cpu_info_give_an_alarm == "on" or shilihua.mem_info_give_an_alarm == "on" or shilihua.disk_info_switch == "on":
        # 获取自身的进程号,用于关闭时使用
        pid = os.getpid()
        # print('进程id : %d' % pid)
        f = open(shilihua.basedir_end + "pids/system_parameter_action.pid", "w")
        f.write(str(pid))
        f.close()