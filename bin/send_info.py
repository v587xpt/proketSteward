# -*- coding: utf-8 -*-
import requests,json,yagmail,configparser
from basics_class import Basics_def

"""
发送平台告警的方法
"""

class Send_info():
    def __init__(self):
        admin = Basics_def().get_path()
        self.file_path = admin

        #这里初始化读取配置文件中的参数

    #企业微信发送info方法
    def send_weChat(self,alarm_info):
        # 读取配置文件
        config = configparser.ConfigParser()
        my_confFile = self.file_path + "conf" + "/" + "send_info.conf"
        config.read(my_confFile, encoding="utf-8")
        corpsecret = config.get('weChat', 'corpsecret')
        corpid = config.get('weChat', 'corpid')
        agentid = config.get('weChat', 'agentid')

        #发送消息
        acquire_access_token_url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s" % (
        corpid, corpsecret)

        request_result = requests.get(url=acquire_access_token_url).json()
        if request_result['errmsg'] == "ok":
            access_token = request_result['access_token']
            # print(access_token)
        else:
            print(request_result['errmsg'])
            exit()

        send_info_destination_url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s" % (access_token)
        data_info = {
           "touser" : "@all",
           "msgtype" : "text",
           "agentid" : agentid,
           "text" : {
               "content" : alarm_info
           },
           "safe":0
        }
        sendInfo_result = requests.post(url=send_info_destination_url,data=json.dumps(data_info)).json()
        # print(sendInfo_result)
        if sendInfo_result['errmsg'] == "ok":
            return "WeChat send success"
        else:
            return sendInfo_result['errmsg']

    #邮件发送info方法
    def send_mail(self,contents):
        #读取配置文件
        config = configparser.ConfigParser()
        my_confFile = self.file_path + "conf" + "/" + "send_info.conf"
        config.read(my_confFile, encoding="utf-8")

        user = config.get('mail','user')
        password = config.get('mail','password')
        host = config.get('mail','host')
        des_mail = config.get('mail','des_mail')
        only_name = config.get('mail','only_name')

        try:
            # 链接邮箱服务器
            smtp = yagmail.SMTP(user=user,password=password,host=host,smtp_ssl=True)
            #发送邮件
            smtp.send(
                to = des_mail,          #发送的目标邮箱
                subject = only_name,    #邮件标题
                contents = contents     #邮件文本内容
            )
        except:
            return "mail send fail."
        else:
            return "mail send success."



if __name__ == "__main__":
    uiui = Send_info()
    uiui.send_weChat("sss")
