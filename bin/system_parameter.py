import psutil

"""编写获取系统cpu、内存、磁盘、网络、IO等方法，用于被调用；"""

class System_item():
    def __init__(self):
        pass

    #获取cpu使用率的方法
    def get_cpu(self, interval):
        #获取所有核心的平均使用率
        cpu_user = psutil.cpu_percent(interval,percpu=False)
        return cpu_user

    #获取内存使用率的方法
    def get_mem(self):
        mem = psutil.virtual_memory()
        """保留小数点后两位,返回 <class 'str'>"""
        # 系统总计内存
        mem_total = '%.2f' % (int(mem.total) / 1024 / 1024 / 1024)
        # 系统已经使用内存
        mem_used = '%.2f' % (int(mem.used) / 1024 / 1024 / 1024)
        # 系统空闲内存
        mem_free = '%.2f' % (int(mem.free) / 1024 / 1024 / 1024)
        # 系统内存使用率
        usage_rate = mem.percent

        return mem_total,mem_used,mem_free,usage_rate

    #获取swap交换分区的方法
    def get_swap(self):
        swap_info = psutil.swap_memory()
        swap_total = '%.2f' % (int(swap_info.total) / 1024 / 1024 / 1024)
        swap_used = '%.2f' % (int(swap_info.used) / 1024 / 1024 / 1024)
        swap_free = '%.2f' % (int(swap_info.free) / 1024 / 1024 / 1024)
        swap_percent = swap_info.percent

        return swap_total,swap_used,swap_free,swap_percent

    #获取磁盘占用的方法
    def get_disk(self,disk_path):
        try:
            disk_info = psutil.disk_usage(disk_path)
        except:
            # return "分区可能填写错误,无法获取分区信息."
            pass
        else:
            disk_total = '%.1f' % (disk_info.total /1024/1024/1024)
            disk_used = '%.1f' % (disk_info.used /1024/1024/1024)
            disk_free = '%.1f' % (disk_info.free /1024/1024/1024)
            disk_percent = disk_info.percent

            return disk_total,disk_used,disk_free,disk_percent

    #获取磁盘IO(目前只适配linux系统)
    def get_diskIO(self):
        diskIo_info = psutil.disk_io_counters(perdisk=True)
        return diskIo_info      #返回字典
    """
        read_count：读取次数
        write_count：写入次数
        read_bytes：读取的字节数
        write_bytes：写入的字节数
        read_time：从磁盘读取的时间（以毫秒为单位）
        write_time：写入磁盘的时间（毫秒为单位）
        read_merged_count：合并读取的数量
        write_merged_count：合并写入次数
        busy_time：花费在实际I/O上的时间
    """

    #获取所有网卡的IO信息
    def get_netIO(self):
        netIO_info = psutil.net_io_counters(pernic=True)
        return netIO_info       #返回字典

    # 获取系统用户
    def get_user(self):
        user_list = []
        users_count = len(psutil.users())   #系统在线用户个数
        for i in psutil.users():
            user_list.append(i.name)
        return users_count,user_list


    #通过进程名字判断是否存在该进程
    def get_pids_name(self,pid_name):
        for i in psutil.pids():
            try:
                pidsd_name = psutil.Process(i).name()

                # print(pidsd_name.name())    #进程名
                # print(pidsd_name.exe())     #进程的bin路径
                # print(pidsd_name.cmdline())
            except:
                pass
            else:
                if pid_name in pidsd_name:
                    return True
                else:
                    return False
    #通过进程号判断进程是否存在
    def get_pids_test(self,pids):   #通过进程号判断进程是否存在
        pids_test = psutil.pid_exists(pids)
        return pids_test
