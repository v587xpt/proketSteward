from system_parameter import System_item
from basics_class import Basics_def
import datetime,time,os
from send_info import Send_info
import subprocess
from prettytable import PrettyTable

"""读取配置文件,监控进程的数量,低于阈值时进行告警"""

class Monitoring_process():
    def __init__(self):
        self.basedir = os.path.abspath(os.path.dirname(__file__))
        self.basedir_end = self.basedir[0:-3]  # 项目路径

        self.system_item = System_item()  # 实例化获取系统参数的类
        self.class_Basics_def = Basics_def()    #实例化基础方法的类

        self.class_Send_info = Send_info()  #实例化发送消息的类

        # 获取发送消息的唯一主机标识
        self.result_send_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/send_info.conf","global")  # 解析哪个文件
        self.only_name = self.result_send_info['only_name']

        # ---获取 process_monitoring 的配置参数
        try:
            self.result_cpu_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/global.conf","process_monitoring")
            self.process_monitoring_switch = self.result_cpu_info["switch"]    #开关
            self.process_monitoring_sendInfo_mode = self.result_cpu_info["send_info_mode"]    #发送告警的类型
            self.process_monitoring_interval = self.result_cpu_info['interval']       #检测时间间隔
            self.process_monitoring_process_name = self.result_cpu_info['process_name'] #要监测的进程
            self.process_monitoring_process_number = self.result_cpu_info['process_number'] #要求的进程数
            self.process_monitoring_sleep_time = self.result_cpu_info['sleep_time'] #告警过之后的休眠时间
        except Exception as e:
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            print(time_stamp + " " + "process_monitoring 解析失败,请检查配置文件参数.")
            exit()


        # 按照逗号分隔成数组
        self.process_name = self.process_monitoring_process_name.split(",")
        self.process_number = self.process_monitoring_process_number.split(",")

        # --异常检测,确保配置文件的正确性
        # 检查使用逗号分割后数组的数量是否相同
        if len(self.process_name) != len(self.process_number):
            print("process_name 和 process_number 配置的数量不一致,请检查配置文件.")
            exit()


#执行shell命令的方法
def run_cmd(shell):
    cp = subprocess.run(shell, shell=True, capture_output=True, encoding='utf-8')
    return cp.stdout,cp.stderr

#判断进程数是否小于阈值的方法
def monitoring_process(process_name):
    #执行shell命令,使用ps命令查询进程的数量
    shell_cmd = "ps -ef | grep -v grep | grep " + str(process_name) + " | wc -l"
    shell_stdout,shell_stderr = run_cmd(shell_cmd)
    return int(shell_stdout)



if __name__ == "__main__":
    shilihua = Monitoring_process()

    if shilihua.process_monitoring_switch == "on":
        # 获取自身的进程号,用于关闭时使用
        pid = os.getpid()
        # print('进程id : %d' % pid)
        f = open(shilihua.basedir_end + "pids/process_monitoring.pid", "w")
        f.write(str(pid))
        f.close()


        for_numbers = len(shilihua.process_name)    #数组内的个数

        tables_1 = shilihua.process_name    #进程名字的数组
        tables_2 = shilihua.process_number  #进程最低数量的数组
        while True:
            tables_3 = []   #存储进程数量的数组

            #1.第一轮for循环,将检测到进程数量的结果存入数组
            for i in range(0,for_numbers):
                process_numbers = monitoring_process(shilihua.process_name[i])  #返回进程的数量
                tables_3.append(int(process_numbers))    #将三个结果存入数组

            #2.第二轮for循环,验证进程数量是否低于阈值
            for q in range(0,len(tables_3)):
                #有任意一个小于阈值,发送告警或输出消息
                if int(tables_3[q]) < int(tables_2[q]):
                    time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
                    print(str(time_stamp) + "    进程数小于阈值,详细信息:")
                    table = PrettyTable()
                    table.add_column('进程字段', tables_1)
                    table.add_column('最低进程数', tables_2)
                    table.add_column('当前进程数', tables_3)
                    print(table)

                    #判断消息类型并发送
                    last_info = "主机：" + shilihua.only_name + "\n" + \
                                "时间：" + time_stamp + "\n" + str(table)
                    if str(shilihua.process_monitoring_sendInfo_mode) == "weChat":  # 发送企业微信告警
                        shilihua.class_Send_info.send_weChat(last_info)
                    if str(shilihua.process_monitoring_sendInfo_mode) == "mail":  # 发送邮件告警
                        shilihua.class_Send_info.send_mail(last_info)

                    time.sleep(int(shilihua.process_monitoring_sleep_time))   #告警过后的休眠,防止频繁发送
                    continue        #跳过本次循环,不再对比后面的进程数,因为已经发现低于阈值的进程了
                else:
                    print("ok")

            time.sleep(int(shilihua.process_monitoring_interval))    #按照检测时间间隔休眠


