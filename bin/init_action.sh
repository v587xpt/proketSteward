#!/bin/bash

#当前脚本的绝对路径
local_script_path=$(dirname $(readlink -f $0))
#echo ${local_script_path}

#python3的bin目录
python3_path_bin=`cd ${local_script_path} && cat ../conf/global.conf | grep python_path | awk -F '=' '{print$2}'`
#echo ${python3_path_bin}


#---如果脚本后的参数是 start 的话
function start(){
  #判断pids中是否还有pid文件,防止重复start开启服务进程(如果有,则exit退出.如果没有,则正常启动.)
  if cd ${local_script_path}/../pids && ls *.pid >> /dev/null 2>&1 ; then   #如果有pid文件
    echo -e "\033[32m服务已经开启,请勿重启启动.\033[0m"
    status
    exit
  fi

  nohup ${python3_path_bin} ${local_script_path}/filtration_log.py >> /dev/null 2>&1 &
  nohup ${python3_path_bin} ${local_script_path}/system_parameter_action.py >> /dev/null 2>&1 &
  nohup ${python3_path_bin} ${local_script_path}/logFile_size.py >> /dev/null 2>&1 &
  nohup ${python3_path_bin} ${local_script_path}/process_monitoring.py >> /dev/null 2>&1 &
}
#---如果脚本后的参数是 stop 的话
function stop(){
    #如果有pid文件
    if cd ${local_script_path}/../pids && ls *.pid >> /dev/null 2>&1 ; then
      for pid_file in `cd ${local_script_path}/../pids && ls *.pid`
      do
        pid=`cat ${local_script_path}/../pids/${pid_file}`
        kill -15 ${pid} && rm -rf ${local_script_path}/../pids/${pid_file}
      done
    #如果没有pid文件
    else
      echo -e "\033[32m服务已经停止,请勿重启关闭.\033[0m"
      status
      exit
    fi
}

function status(){
  #1.filtration_log 进程状态(监控日志关键字)
  if [ ! -f "${local_script_path}/../pids/filtration_log.pid" ]; then
    #先判断pid文件是否存在,不存在则为[关闭]状态
    echo -e "filtration_log 进程状态 ------------ [\033[31m关闭\033[0m]"
  else
    filtration_log_pid=`cd ${local_script_path} && cat ../pids/filtration_log.pid`
    if [ `ps -p ${filtration_log_pid} | wc -l` -gt 1 ]; then
      echo -e "filtration_log 进程状态 ------------ [\033[32m运行\033[0m]"
    else
      echo -e "filtration_log 进程状态 ------------ [\033[31m关闭\033[0m]"
    fi
  fi

  #2.system_parameter 进程状态(cpu/mem/disk监控)
  if [ ! -f "${local_script_path}/../pids/system_parameter_action.pid" ]; then
    echo -e "system_parameter 进程状态 ---------- [\033[31m关闭\033[0m]"
  else
    system_parameter_action_pid=`cd ${local_script_path} && cat ../pids/system_parameter_action.pid`
    if [ `ps -p ${system_parameter_action_pid} | wc -l` -gt 1 ]; then
      echo -e "system_parameter 进程状态 ---------- [\033[32m运行\033[0m]"
    else
      echo -e "system_parameter 进程状态 ---------- [\033[31m关闭\033[0m]"
    fi
  fi

  #3.logFile_size 进程状态(监控日志大小,日志切割)
  if [ ! -f "${local_script_path}/../pids/logFile_size.pid" ]; then
    echo -e "logFile_size 进程状态 -------------- [\033[31m关闭\033[0m]"
  else
    logFile_size_pid=`cd ${local_script_path} && cat ../pids/logFile_size.pid`
    if [ `ps -p ${logFile_size_pid} | wc -l` -gt 1 ]; then
      echo -e "logFile_size 进程状态 -------------- [\033[32m运行\033[0m]"
    else
      echo -e "logFile_size 进程状态 -------------- [\033[31m关闭\033[0m]"
    fi
  fi

  #4.process_monitoring 进程状态(监控指定进程)
  if [ ! -f "${local_script_path}/../pids/process_monitoring.pid" ]; then
    echo -e "process_monitoring 进程状态 -------- [\033[31m关闭\033[0m]"
  else
    process_monitoring_pid=`cd ${local_script_path} && cat ../pids/process_monitoring.pid`
    if [ `ps -p ${process_monitoring_pid} | wc -l` -gt 1 ]; then
      echo -e "process_monitoring 进程状态 -------- [\033[32m运行\033[0m]"
    else
      echo -e "process_monitoring 进程状态 -------- [\033[31m关闭\033[0m]"
    fi
  fi
}


#判断脚本后的参数个数
if [ $# != 1 ]; then
	echo -e "\033[32mPlease execute correctly:\033[0m"
	echo -e "   \033[33m|\033[0m"
	echo -e "   \033[33m|___sh $0 start\033[0m"
	echo -e "   \033[33m|   |___启动服务\033[0m"
	echo -e "   \033[33m|\033[0m"
	echo -e "   \033[33m|___sh $0 stop\033[0m"
	echo -e "   \033[33m|   |___关闭服务\033[0m"
	echo -e "   \033[33m|\033[0m"
	echo -e "   \033[33m|___sh $0 status\033[0m"
  echo -e "   \033[33m    |___查看服务状态\033[0m"
	exit
fi

case $1 in
	start)
	echo -e "\033[33m启动服务\033[0m"
	start

	echo "启动中."
	sleep 1
	echo "启动中.."
	sleep 1
	echo "启动中..."
	sleep 1

	status
	;;

	stop)
	echo -e "\033[33m关闭服务\033[0m"
	stop

	echo "关闭中."
	sleep 1
	echo "关闭中.."
	sleep 1
	echo "关闭中..."
	sleep 1

	status
	;;

	status)
	echo -e "\033[33m检查服务状态\033[0m"
	status
	;;

	*)
	echo -e "\033[32mPlease execute correctly:\033[0m"
	echo -e "   \033[33m|\033[0m"
	echo -e "   \033[33m|___sh $0 start\033[0m"
	echo -e "   \033[33m|   |___启动服务\033[0m"
	echo -e "   \033[33m|\033[0m"
	echo -e "   \033[33m|___sh $0 stop\033[0m"
	echo -e "   \033[33m|   |___关闭服务\033[0m"
	echo -e "   \033[33m|\033[0m"
	echo -e "   \033[33m|___sh $0 status\033[0m"
  echo -e "   \033[33m    |___查看服务状态\033[0m"
	;;
esac