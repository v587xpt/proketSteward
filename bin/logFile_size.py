import datetime, time, os
from basics_class import Basics_def

"""
功能:对指定的日志文件进行监视,达到预定大小后进行 备份/新建 等操作,实现日志文件的切割
"""


class LogFile_size():
    def __init__(self):
        self.basedir = os.path.abspath(os.path.dirname(__file__))
        self.basedir_end = self.basedir[0:-3]  # 项目路径

        # 获取配置文件中的参数
        self.class_Basics_def = Basics_def()  # 实例化基础方法的类
        self.result_send_info = self.class_Basics_def.analysis_parameter(self.basedir_end + "conf/global.conf","log_file_cutting")  # 解析配置文件
        self.logFile_switch = self.result_send_info['switch']
        self.logFile_interval = self.result_send_info['interval']
        self.logFile_log_path = self.result_send_info['log_path']
        self.logFile_log_size = self.result_send_info['log_size']

    def main(self):
        # 按照逗号分隔成数组
        log_path_list = self.logFile_log_path.split(",")
        log_size_list = self.logFile_log_size.split(",")

        # --异常检测,确保配置文件的正确性
        # 1.检查检测的日志文件数量和size数量是否相同
        if len(log_path_list) != len(log_size_list):
            print("log_path 和 log_size 配置的数量不一致,请检查配置文件.")
            exit()
        # 2.检测配置文件中的日志文件是否存在
        for file in log_path_list:
            result_isfile = os.path.isfile(file)
            if result_isfile == False:
                print(str(file) + " 目标文件不存在,请检查配置文件是否正确.")
                exit()

        while True:
            # 获取目标文件的大小
            for nunu in range(len(log_path_list)):
                # 获取文件的大小；
                fsize = os.path.getsize(log_path_list[nunu]) / 1024 / 1024
                fsize = round(fsize, 2)  # 小数点后两位
                # print(fsize)

                if fsize > int(log_size_list[nunu]):
                    # 超过阈值,进行文件分割
                    src = log_path_list[nunu]
                    time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')  # 获取当前时间
                    dst = log_path_list[nunu] + "_" + time_stamp
                    os.rename(src, dst)  # 修改文件名字备份文件

                    open_new_file = open(log_path_list[nunu], "w")  # 新建一个原文件
                    open_new_file.close()

            time.sleep(int(self.logFile_interval))  # 检测时间间隔---休眠


if __name__ == '__main__':

    shilihua = LogFile_size()       #class类实例化

    if shilihua.logFile_switch == "on":
        # 获取自身的进程号,用于关闭时使用
        pid = os.getpid()
        # print('进程id : %d' % pid)
        f = open(shilihua.basedir_end + "pids/logFile_size.pid", "w")
        f.write(str(pid))
        f.close()

        # 运行main方法
        shilihua.main()
