import time
from prettytable import PrettyTable

# table = PrettyTable(['编号','java进程','mysql进程','nginx进程'])
# table.add_row(['1','2','3','172.16.0.1'])
# table.add_row(['2','server02','服务器02','172.16.0.2'])
# print(table)

tables_1 = ['java进程','mysql进程','nginx进程']
tables_2 = [2,2,1]
tables_3 = [2,1,0]


while True:
    table = PrettyTable()
    table.add_column('进程字段', tables_1)
    table.add_column('最低进程数', tables_2)
    table.add_column('现在进程数', tables_3)
    time.sleep(1)

    print(table)

