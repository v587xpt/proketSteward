import os,datetime,configparser
# -*- coding: utf-8 -*-

class Basics_def():
    def __init__(self):
        pass

    # 获取项目绝对路径
    def get_path(self):
        basedir = os.path.abspath(os.path.dirname(__file__))
        basedir_end = basedir[0:-3]
        return basedir_end

    # 获取当前时间的方法
    def get_time(self):
        time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
        return time_stamp

    # 判断文件是否存在
    def get_isfile(self,file_path):
        result_isfile = os.path.isfile(file_path)  # 判断文件是否存在
        return result_isfile

    #读取conf配置文件的方法
    def analysis_parameter(self,file_path, options):
        dic_parameter = {}
        # 读取配置文件
        config = configparser.ConfigParser()
        config.read(file_path, encoding="utf-8")
        for i in config.options(options):  # 遍历配置文件内的选项[]
            parameter = config.get(options, i)
            if "\"" in parameter:  # 判断是否有双引号,并进行分割
                dic_parameter[i] = parameter.split("\"")[1].strip()
            else:
                dic_parameter[i] = parameter.split("#")[0].strip()
        return dic_parameter


if __name__ == "__main__":
    admin = Basics_def()
    result = admin.analysis_parameter(r"E:\file_data\my_project\eye-of-overlook\conf\system_parameter.conf","mem_info")

    print(result)
