import time,subprocess,datetime
from bin.send_info import Send_info
# -*- coding: utf-8 -*-

"""旧版的监控文件,此处用于保留使用"""

# 执行shell命令的方法，调用Exec即可;
def runWithOutLog(cmd):
    p = subprocess.Popen(cmd, shell=True, close_fds=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    return (p.returncode, stdout, stderr)
def run(cmd):
    returncode, stdout, stderr = runWithOutLog(cmd)
    return (returncode, stdout.strip(), stderr.strip())
def Exec(cmd):
    ISOTIMEFORMAT = '%Y-%m-%d %X'
    reportTime = time.strftime(ISOTIMEFORMAT, time.localtime())
    retcode, stdout, stderr = run(cmd)
    print('[%s] exec cmd[%s] stdout[%s] stderr[%s]' % (reportTime, cmd, str(stdout), str(stderr)))
    # if retcode:
    #     raise Exception, "exec cmd[%s] failed, stdout[%s], error[%s]" % (cmd, stdout, stderr)
    return stdout, stderr
def Exectt(cmd):
    ISOTIMEFORMAT = '%Y-%m-%d %X'
    reportTime = time.strftime(ISOTIMEFORMAT, time.localtime())
    retcode, stdout, stderr = run(cmd)
    # print ('[%s] exec cmd[%s]' % (reportTime, cmd))
    return stdout.decode('utf-8'), stderr

# 获取指定文件的最后一行的行号
def get_lines_new(file_path):
    stdout,stderr = Exectt("cat %s | wc -l" % (file_path))
    return stdout


if __name__ == "__main__":
    only_name = "主机测试"
    file_path = "nohup.out"
    interval_time = 7       #检测的时间间隔


    #先进行一次行号检测(前提是文件必须存在,进入循环后被检测文件可以不存在)
    start_numbers = get_lines_new(file_path)  # 初次检测，获取行号；
    time.sleep(interval_time)
    stop_numbers = get_lines_new(file_path)  # 休眠后再次检测，获取行号；

    #进入循环
    while True:
        #只有当 start_numbers<stop_numbers 时才进行新增内容的获取判断
        if int(start_numbers) < int(stop_numbers):
            dic_new_info = {}  # 定义字典，存储文件变动的内容，行号是键，内容是值；
            # 获取变动的内容
            out_yes, out_no = Exectt("sed -n '%s,%sp' %s" % (int(start_numbers)+1, int(stop_numbers), file_path))
            # 将变动的 内容和行号 存入字典，行号是键，内容是值；
            hangHao = int(start_numbers)+1
            for i in out_yes.split('\n'):
                dic_new_info[hangHao] = i
                hangHao += 1
            # print(dic_new_info)     #输出编排后的字典数据


            list_new_info = []      #定义数组，存储含有关键字的行，以及其上下行
            for iu in dic_new_info:     #遍历字典
                #判断关键字
                if "Exception" in dic_new_info[iu] and "调用" not in dic_new_info[iu] and "customException" not in dic_new_info[iu] and "IllegalArgumentException" not in dic_new_info[iu] and "SocketTimeoutException" not in dic_new_info[iu]:
                    """一旦过滤到匹配行，就将该行连同上下行一起存储到数组中"""
                    if iu-1 not in list_new_info:
                        list_new_info.append(iu-1)
                    if iu not in list_new_info:
                        list_new_info.append(iu)
                    if iu+1 not in list_new_info:
                        list_new_info.append(iu+1)
                else:   #过滤不到定义的字符串，则pass通过
                    pass
            # print(list_new_info)


            if len(list_new_info) != 0:
                #读取这些行，并组成段落
                error_info = ""
                for inum in list_new_info:
                    m_one_yes, m_one_no = Exectt("sed -n '%s,%sp' %s" % (inum, inum, file_path))
                    error_info = error_info + "【" + str(inum) + "行】" + " " + m_one_yes + "\n"

                time_stamp = datetime.datetime.now()
                last_info = "主机：" + only_name + "\n" + \
                            "时间：" + time_stamp.strftime('%Y.%m.%d-%H:%M:%S') + "\n" + \
                            "告警消息：" + "\n" +error_info
                #输出或者发送告警
                print(last_info)


            #更新行号
            try:
                start_numbers = int(stop_numbers)  # 更新初始行号
                time.sleep(interval_time)
                stop_numbers = get_lines_new(file_path)  # 休眠后重新获取新的行号
            except:
                #当检测中出现报错时,通过定义错误的值,引导程序进入else中
                start_numbers = 0
                stop_numbers = 0

        else:
            time_stamp = datetime.datetime.now().strftime('%Y.%m.%d-%H:%M:%S')
            print(time_stamp + " " + "不满足判断条件,重新检测...")
            try:
                start_numbers = get_lines_new(file_path)  # 更新初始行号
                time.sleep(interval_time)
                stop_numbers = get_lines_new(file_path)  # 休眠后重新获取新的行号
            except:
                # 当检测中出现报错时,通过定义错误的值,引导程序进入else中
                start_numbers = 0
                stop_numbers = 0